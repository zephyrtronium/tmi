/*
Copyright 2022 Branden J Brown

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package tmi_test

import (
	"strings"
	"testing"

	"github.com/google/go-cmp/cmp"
	"gitlab.com/zephyrtronium/tmi"
)

func TestString(t *testing.T) {
	// We're only really interested in correct formatting of messages we send,
	// so we don't need to test sender.
	cases := []struct {
		in  tmi.Message
		out string
	}{
		{tmi.Message{Command: "PRIVMSG"}, "PRIVMSG"},
		{tmi.Message{Command: "PRIVMSG", Params: []string{"#madoka"}}, "PRIVMSG #madoka"},
		{tmi.Message{Command: "PRIVMSG", Params: []string{"#madoka", "#homura"}}, "PRIVMSG #madoka #homura"},
		{tmi.Message{Command: "PRIVMSG", Trailing: "anime"}, "PRIVMSG :anime"},
		{tmi.Message{Command: "PRIVMSG", Trailing: "anime madoka homura"}, "PRIVMSG :anime madoka homura"},
		{tmi.Message{Command: "PRIVMSG", Params: []string{"#madoka"}, Trailing: "anime"}, "PRIVMSG #madoka :anime"},
		{tmi.Message{Command: "PRIVMSG", Params: []string{"#madoka", "#homura"}, Trailing: "anime"}, "PRIVMSG #madoka #homura :anime"},
		{tmi.Message{Command: "PRIVMSG", Params: []string{"#madoka"}, Trailing: "anime madoka homura"}, "PRIVMSG #madoka :anime madoka homura"},
		{tmi.Message{Command: "PRIVMSG", Params: []string{"#madoka", "#homura"}, Trailing: "anime madoka homura"}, "PRIVMSG #madoka #homura :anime madoka homura"},
		{tmi.Message{Tags: "a=b;c", Command: "PRIVMSG"}, "@a=b;c PRIVMSG"},
		{tmi.Message{Tags: "a=b;c", Command: "PRIVMSG", Params: []string{"#madoka"}}, "@a=b;c PRIVMSG #madoka"},
		{tmi.Message{Tags: "a=b;c", Command: "PRIVMSG", Params: []string{"#madoka", "#homura"}}, "@a=b;c PRIVMSG #madoka #homura"},
		{tmi.Message{Tags: "a=b;c", Command: "PRIVMSG", Trailing: "anime"}, "@a=b;c PRIVMSG :anime"},
		{tmi.Message{Tags: "a=b;c", Command: "PRIVMSG", Trailing: "anime madoka homura"}, "@a=b;c PRIVMSG :anime madoka homura"},
		{tmi.Message{Tags: "a=b;c", Command: "PRIVMSG", Params: []string{"#madoka"}, Trailing: "anime"}, "@a=b;c PRIVMSG #madoka :anime"},
		{tmi.Message{Tags: "a=b;c", Command: "PRIVMSG", Params: []string{"#madoka", "#homura"}, Trailing: "anime"}, "@a=b;c PRIVMSG #madoka #homura :anime"},
		{tmi.Message{Tags: "a=b;c", Command: "PRIVMSG", Params: []string{"#madoka"}, Trailing: "anime madoka homura"}, "@a=b;c PRIVMSG #madoka :anime madoka homura"},
		{tmi.Message{Tags: "a=b;c", Command: "PRIVMSG", Params: []string{"#madoka", "#homura"}, Trailing: "anime madoka homura"}, "@a=b;c PRIVMSG #madoka #homura :anime madoka homura"},
	}
	for _, c := range cases {
		t.Run(c.out, func(t *testing.T) {
			s := c.in.String()
			if s != c.out {
				t.Errorf("wrong message: expected %q, got %q", c.out, s)
			}
		})
	}
}

func TestTags(t *testing.T) {
	type try struct {
		tag string
		val string
		ok  bool
	}
	cases := []struct {
		tags string
		try  []try
	}{
		{`a`, []try{
			{"a", "", true},
			{"b", "", false},
			{"", "", false},
		}},
		{`a=`, []try{
			{"a", "", true},
			{"b", "", false},
			{"", "", false},
		}},
		{`a=b`, []try{
			{"a", "b", true},
			{"b", "", false},
			{"", "", false},
		}},
		{`a=\:\s\\\r\n\t\x00`, []try{
			{"a", "; \\\r\ntx00", true},
			{"b", "", false},
			{"t", "", false},
			{"x", "", false},
			{"0", "", false},
			{"", "", false},
		}},
		{`a=\`, []try{
			{"a", "", true},
			{"b", "", false},
			{"", "", false},
		}},
		{`a;c`, []try{
			{"a", "", true},
			{"c", "", true},
			{"", "", false},
		}},
		{`a=;c`, []try{
			{"a", "", true},
			{"c", "", true},
			{"", "", false},
		}},
		{`a;c=`, []try{
			{"a", "", true},
			{"c", "", true},
			{"", "", false},
		}},
		{`a=;c=`, []try{
			{"a", "", true},
			{"c", "", true},
			{"", "", false},
		}},
		{`a=b;c`, []try{
			{"a", "b", true},
			{"b", "", false},
			{"c", "", true},
			{"", "", false},
		}},
		{`a=b;c=`, []try{
			{"a", "b", true},
			{"b", "", false},
			{"c", "", true},
			{"", "", false},
		}},
		{`a;c=d`, []try{
			{"a", "", true},
			{"b", "", false},
			{"c", "d", true},
			{"", "", false},
		}},
		{`a=;c=d`, []try{
			{"a", "", true},
			{"b", "", false},
			{"c", "d", true},
			{"", "", false},
		}},
		{`a=b;c=d`, []try{
			{"a", "b", true},
			{"b", "", false},
			{"c", "d", true},
			{"", "", false},
		}},
		{`a=\:\s\\\r\n\t\x00;c=d`, []try{
			{"a", "; \\\r\ntx00", true},
			{"b", "", false},
			{"t", "", false},
			{"x", "", false},
			{"0", "", false},
			{"c", "d", true},
			{"", "", false},
		}},
		{`a=\;c=d`, []try{
			{"a", "", true},
			{"b", "", false},
			{"c", "d", true},
			{"", "", false},
		}},
	}
	for _, c := range cases {
		t.Run(c.tags, func(t *testing.T) {
			m := tmi.Message{Tags: c.tags, Command: "PRIVMSG"}
			for _, c := range c.try {
				t.Run(c.tag, func(t *testing.T) {
					r, ok := m.Tag(c.tag)
					if ok != c.ok {
						t.Errorf("tag parse success mismatch, expected %v, got %v", c.ok, ok)
					}
					if r != c.val {
						t.Errorf("tag value mismatch, expected %q, got %q", c.val, r)
					}
				})
			}
		})
	}
}

func TestBadges(t *testing.T) {
	cases := []struct {
		b string
		r []string
	}{
		{"", nil},
		{"a/0", []string{"a"}},
		{"a/0,b/0", []string{"a", "b"}},
	}
	for _, c := range cases {
		t.Run(c.b, func(t *testing.T) {
			m := tmi.Message{Tags: "badges=" + c.b, Command: "PRIVMSG"}
			r := m.Badges(nil)
			if diff := cmp.Diff(c.r, r); diff != "" {
				t.Errorf("wrong badges (-want +got):\n%s", diff)
			}
		})
	}
}

func TestDisplayName(t *testing.T) {
	cases := []struct {
		name string
		msg  tmi.Message
		n    string
	}{
		{"none", tmi.Message{Tags: "", Sender: tmi.Sender{Nick: "nick"}}, "nick"},
		{"empty", tmi.Message{Tags: "display-name=", Sender: tmi.Sender{Nick: "nick"}}, "nick"},
		{"provided", tmi.Message{Tags: "display-name=NICK", Sender: tmi.Sender{Nick: "nick"}}, "NICK"},
	}
	for _, c := range cases {
		t.Run(c.msg.Tags, func(t *testing.T) {
			if got := c.msg.DisplayName(); got != c.n {
				t.Errorf("wrong display name: want %q, got %q", c.n, got)
			}
		})
	}
}

func TestRespond(t *testing.T) {
	cases := []struct {
		to   *tmi.Message
		msg  string
		args []interface{}
		resp *tmi.Message
	}{
		{qp(":anime!anime@anime.tmi.twitch.tv PRIVMSG #test :madoka"), "homura", nil, qp("PRIVMSG #test :homura")},
		{qp(":anime!anime@anime.tmi.twitch.tv PRIVMSG #test :madoka"), "@%s homura", []interface{}{"anime"}, qp("PRIVMSG #test :@anime homura")},
		{qp("@id=abcd :anime!anime@anime.tmi.twitch.tv PRIVMSG #test :madoka"), "homura", nil, qp("PRIVMSG #test :homura")},
		{qp("@id=abcd :anime!anime@anime.tmi.twitch.tv PRIVMSG #test :madoka"), "@animem homura", nil, qp("PRIVMSG #test :@animem homura")},
		{qp("@id=abcd;display-name=Anime :anime!anime@anime.tmi.twitch.tv PRIVMSG #test :madoka"), "@%s homura", []interface{}{"anime"}, qp("PRIVMSG #test :@anime homura")},
		{qp("@id=abcd :anime!anime@anime.tmi.twitch.tv PRIVMSG #test :madoka"), "@%s homura", []interface{}{"anime"}, qp("PRIVMSG #test :@anime homura")},
		{qp(":anime!anime@anime.tmi.twitch.tv WHISPER kyuubei :madoka"), "homura", nil, qp("PRIVMSG #jtv :/w anime homura")},
		{qp("@message-id=13 :anime!anime@anime.tmi.twitch.tv WHISPER kyuubei :madoka"), "@%s homura", []interface{}{"anime"}, qp("PRIVMSG #jtv :/w anime @anime homura")},
	}
	for _, c := range cases {
		r := c.to.Respond(c.msg, c.args...)
		if diff := cmp.Diff(c.resp, r); diff != "" {
			t.Errorf("%q -> %q gave wrong reply (-want +got):\n%s", c.to.String(), c.msg, diff)
		}
	}
}

func TestReply(t *testing.T) {
	cases := []struct {
		to   *tmi.Message
		msg  string
		args []interface{}
		resp *tmi.Message
	}{
		{qp(":anime!anime@anime.tmi.twitch.tv PRIVMSG #test :madoka"), "homura", nil, qp("PRIVMSG #test :homura")},
		{qp(":anime!anime@anime.tmi.twitch.tv PRIVMSG #test :madoka"), "@%s homura", []interface{}{"anime"}, qp("PRIVMSG #test :@anime homura")},
		{qp("@id=abcd :anime!anime@anime.tmi.twitch.tv PRIVMSG #test :madoka"), "homura", nil, qp("@reply-parent-msg-id=abcd PRIVMSG #test :homura")},
	}
	for _, c := range cases {
		r := c.to.Reply(c.msg, c.args...)
		if diff := cmp.Diff(c.resp, r); diff != "" {
			t.Errorf("%q -> %q gave wrong reply (-want +got):\n%s", c.to.String(), c.msg, diff)
		}
	}
}

func qp(s string) *tmi.Message {
	r, err := tmi.Parse(strings.NewReader(s + "\r\n"))
	if err != nil {
		panic(err)
	}
	return r
}
