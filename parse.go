/*
Copyright 2022 Branden J Brown

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package tmi

import (
	"io"
	"strings"
)

// Parse parses a message.
func Parse(scan io.RuneScanner) (*Message, error) {
	var (
		msg Message
		eol bool
		err error
	)
	msg.Tags, err = scanTags(scan)
	if err != nil {
		return nil, err
	}
	msg.Sender, err = scanSender(scan)
	if err != nil {
		return nil, err
	}
	// Parse command.
	msg.Command, eol, err = scanField(scan, "command", ircLimit)
	if err != nil {
		return nil, err
	}
	if eol {
		return &msg, nil
	}
	// Parse middle args.
	for {
		r, _, err := scan.ReadRune()
		if err != nil {
			return nil, err
		}
		if r == ':' {
			break
		}
		scan.UnreadRune()
		arg, eol, err := scanField(scan, "middle", ircLimit)
		if err != nil {
			return nil, err
		}
		if arg != "" {
			msg.Params = append(msg.Params, arg)
		}
		if eol {
			return &msg, nil
		}
	}
	// Parse trailing.
	msg.Trailing, err = scanRest(scan, "trailing")
	if err != nil {
		return nil, err
	}
	return &msg, nil
}

// tagLimit is the maximum length of a tag in runes.
const tagLimit = 8192

// ircLimit is the maximum length of an IRC message, excluding tag, in runes.
const ircLimit = 512

func unquoteTag(value string) string {
	// We try hard to avoid a copy. We already know the tag value is
	// well-formed because it was successfully parsed, so we can return a
	// substring as long as the string contains no escape sequences.
	for k, r := range value {
		switch r {
		case ';':
			return value[:k]
		case '\\':
			return unescapeTag(value[:k], value[k:])
		}
	}
	return value
}

func unescapeTag(raw, quoted string) string {
	var b strings.Builder
	b.WriteString(raw)
	q := false
	for _, r := range quoted {
		if q {
			// Unescape backslash sequence. The sequences are:
			//	\: -> ';'
			//	\s -> ' '
			//	\\ -> '\'
			//	\r -> CR
			//	\n -> LF
			// Any other sequence causes the backslash to be ignored (without error).
			switch r {
			case ':':
				b.WriteByte(';')
			case 's':
				b.WriteByte(' ')
			case 'r':
				b.WriteByte('\r')
			case 'n':
				b.WriteByte('\n')
			default:
				b.WriteRune(r)
			}
			q = false
			continue
		}
		switch r {
		case ';':
			return b.String()
		case '\\':
			q = true
		default:
			b.WriteRune(r)
		}
	}
	return b.String()
}

func scanTags(scan io.RuneScanner) (string, error) {
	r, _, err := scan.ReadRune()
	switch {
	case err != nil:
		return "", err
	case r != '@':
		scan.UnreadRune()
		return "", nil
	}
	tags, eol, err := scanField(scan, "tags", tagLimit)
	if err != nil {
		return "", err
	}
	if eol {
		return "", &Malformed{stage: "message (only has tags)"}
	}
	return tags, nil
}

func scanSender(scan io.RuneScanner) (Sender, error) {
	r, _, err := scan.ReadRune()
	switch {
	case err != nil:
		return Sender{}, err
	case r != ':':
		scan.UnreadRune()
		return Sender{}, nil
	}
	var (
		b strings.Builder
		s Sender
	)
	cur := &s.Nick
	for i := 0; i < ircLimit; i++ {
		r, _, err := scan.ReadRune()
		if err != nil {
			return Sender{}, err
		}
		switch r {
		case '!':
			// nick into user
			s.Nick = b.String()
			b.Reset()
			cur = &s.User
		case '@':
			// nick or user into server
			*cur = b.String()
			b.Reset()
			cur = &s.Host
		case ' ':
			// nick, user, or server into finish
			*cur = b.String()
			if *cur == "" {
				return Sender{}, &Malformed{stage: "sender"}
			}
			if err := eatSpace(scan); err != nil {
				return Sender{}, err
			}
			return s, nil
		case '\r', '\n', '\000':
			return Sender{}, &Malformed{stage: "sender"}
		default:
			b.WriteRune(r)
		}
	}
	return Sender{}, &Malformed{stage: "sender"}
}

// scanField scans a single space-separated field and skips the space following
// it. If eol is true, then the field was the end of the message and the next
// rune is \n.
func scanField(scan io.RuneScanner, stage string, limit int) (field string, eol bool, err error) {
	var (
		b strings.Builder
		r rune
	)
	for i := 0; i < limit; i++ {
		r, _, err = scan.ReadRune()
		if err != nil {
			return b.String(), false, err
		}
		switch r {
		case ' ':
			if err := eatSpace(scan); err != nil {
				return b.String(), false, err
			}
			eol, err := checkeol(scan, stage)
			return b.String(), eol, err
		case '\r', '\n':
			// We know we're at EOL, but checkeol will tell us if \r is bad.
			scan.UnreadRune()
			eol, err := checkeol(scan, stage)
			return b.String(), eol, err
		case '\000':
			return "", false, &Malformed{stage: stage}
		default:
			b.WriteRune(r)
		}
	}
	return "", false, &Malformed{stage: stage}
}

// scanRest scans through the end of the line. The returned string does not
// include the line terminator.
func scanRest(scan io.RuneScanner, stage string) (line string, err error) {
	var b strings.Builder
	for i := 0; i < ircLimit; i++ {
		r, _, err := scan.ReadRune()
		if err != nil {
			return b.String(), err
		}
		switch r {
		case '\r':
			r, _, err := scan.ReadRune()
			if err != nil {
				return b.String(), err
			}
			if r != '\n' {
				return b.String(), &Malformed{stage: stage}
			}
			return b.String(), nil
		case '\n':
			return b.String(), nil
		case '\000':
			return "", &Malformed{stage: stage}
		default:
			b.WriteRune(r)
		}
	}
	return "", &Malformed{stage: stage}
}

// eatSpace scans until the next character that is not U+0020 and unreads it.
func eatSpace(scan io.RuneScanner) error {
	var (
		r   rune
		err error
	)
	for i := 0; i < ircLimit; i++ {
		r, _, err = scan.ReadRune()
		if err != nil {
			return err
		}
		switch r {
		case ' ':
			continue
		case '\000':
			return &Malformed{stage: "space"}
		default:
			return scan.UnreadRune()
		}
	}
	return &Malformed{stage: "space"}
}

// checkeol checks whether the scanner is exactly at the end of a line. If it
// is, then the scanner is advanced past it.
func checkeol(scan io.RuneScanner, stage string) (bool, error) {
	r, _, err := scan.ReadRune()
	if err != nil {
		return false, err
	}
	switch r {
	case '\r':
		r, _, err := scan.ReadRune()
		if err != nil {
			return false, err
		}
		if r != '\n' {
			return false, &Malformed{stage: stage}
		}
		return true, nil
	case '\n':
		return true, nil
	case '\000':
		return false, &Malformed{stage: stage}
	default:
		scan.UnreadRune()
		return false, nil
	}
}

// Malformed indicates a malformed IRC message.
type Malformed struct {
	stage string
}

func (err *Malformed) Error() string {
	return "malformed " + err.stage
}
