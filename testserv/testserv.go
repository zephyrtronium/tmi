package testserv

import (
	"bufio"
	"io"
	"net"
	"time"
)

// Conn is a line-oriented mock connection for testing IRC parsing.
type Conn struct {
	rmu chan struct{}
	wmu chan struct{}
	s   chan struct{}
	c   chan struct{}

	r *bufio.Reader
	w io.Writer

	closes int
}

// New creates a mock connection reading from r and writing to w.
// The caller must write to the connection to allow reads to begin.
// To spy on written data, the caller should retain w and ensure that a call
// to Close happens before accessing it.
func New(r io.Reader, w io.Writer) *Conn {
	return &Conn{
		rmu: make(chan struct{}, 1),
		wmu: make(chan struct{}, 1),
		s:   make(chan struct{}),
		c:   make(chan struct{}),
		r:   bufio.NewReader(r),
		w:   w,
	}
}

func (s *Conn) runlock() { s.rmu <- struct{}{} }
func (s *Conn) rlock()   { <-s.rmu }
func (s *Conn) wunlock() { <-s.wmu }
func (s *Conn) wlock()   { s.wmu <- struct{}{} }

// Read fills b up to its length or the first line feed, whichever comes first,
// from the reader passed to [New]. If the conn is already closed, Read
// immediately returns [net.ErrClosed]. If the underlying reader is at EOF,
// then Read acts as if the connection is closed.
//
// Calls to Read block until the first call to Write.
func (s *Conn) Read(b []byte) (n int, err error) {
	// First wait for the first write.
	select {
	case <-s.s: // do nothing
	case <-s.c:
		return 0, net.ErrClosed
	}
	select {
	case <-s.rmu:
		defer s.runlock()
		// Always check whether the conn is closed.
		select {
		case <-s.c:
			return 0, net.ErrClosed
		default: // do nothing
		}
	case <-s.c:
		return 0, net.ErrClosed
	}
	for n = range b {
		c, err := s.r.ReadByte()
		switch err {
		case nil:
			b[n] = c
			if c == '\n' {
				return n + 1, nil
			}
		case io.EOF:
			if n != 0 {
				return n, nil
			}
			// Close the connection.
			s.closes++
			close(s.c)
			return 0, net.ErrClosed
		default:
			return n, err
		}
	}
	return n + 1, nil
}

// Write writes to the writer passed to [New]. Writes always proceed.
// The first call to Write happens-before any call to Read completes.
func (s *Conn) Write(b []byte) (n int, err error) {
	s.wlock()
	defer s.wunlock()
	// If this is the first write, allow reads to proceed.
	select {
	case <-s.s:
		// Already started. Do nothing.
	default:
		s.runlock()
		close(s.s)
	}
	return s.w.Write(b)
}

// Close causes all current and future Read or Write calls to return
// [net.ErrClosed].
func (s *Conn) Close() error {
	s.rlock()
	defer s.runlock()
	s.closes++
	if s.closes != 1 {
		return net.ErrClosed
	}
	close(s.c)
	return nil
}

// LocalAddr does nothing and returns nil.
func (s *Conn) LocalAddr() net.Addr { return nil }

// RemoteAddr does nothing and returns nil.
func (s *Conn) RemoteAddr() net.Addr { return nil }

// SetDeadline does nothing and returns nil.
func (s *Conn) SetDeadline(t time.Time) error { return nil }

// SetReadDeadline does nothing and returns nil.
func (s *Conn) SetReadDeadline(t time.Time) error { return nil }

// SetWriteDeadline does nothing and returns nil.
func (s *Conn) SetWriteDeadline(t time.Time) error { return nil }
