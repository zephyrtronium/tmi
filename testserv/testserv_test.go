package testserv_test

import (
	"bytes"
	"io"
	"net"
	"strings"
	"testing"

	"gitlab.com/zephyrtronium/tmi/testserv"
)

func start(c *testserv.Conn) *testserv.Conn {
	c.Write(nil)
	return c
}

func TestRead(t *testing.T) {
	cases := []struct {
		name string
		text string
		len  int
		read string
		err  error
	}{
		{
			name: "single",
			text: "b",
			len:  1,
			read: "b",
			err:  nil,
		},
		{
			name: "short",
			text: "bocchi the rock!",
			len:  6,
			read: "bocchi",
			err:  nil,
		},
		{
			name: "full",
			text: "bocchi",
			len:  64,
			read: "bocchi",
			err:  nil,
		},
		{
			name: "newline",
			text: "bocchi\r\nryou\r\nnijika\r\nkita\r\n",
			len:  64,
			read: "bocchi\r\n",
			err:  nil,
		},
		{
			name: "eof",
			text: "",
			len:  1,
			read: "",
			err:  net.ErrClosed,
		},
	}
	for _, c := range cases {
		c := c
		t.Run(c.name, func(t *testing.T) {
			r := strings.NewReader(c.text)
			conn := start(testserv.New(r, io.Discard))
			b := make([]byte, c.len)
			n, err := conn.Read(b)

			if err != c.err {
				t.Errorf("wrong error: want %v, got %v", c.err, err)
			}
			if n != len(c.read) {
				t.Errorf("wrong length: want %d, got %d", len(c.read), n)
			}
			if string(b[:n]) != c.read {
				t.Errorf("wrong read: want %q, got %q", c.read, b[:n])
			}
		})
	}
}

func TestCloseHappensBeforeRead(t *testing.T) {
	r := bytes.NewBufferString("bocchi")
	c := start(testserv.New(r, io.Discard))
	b := make([]byte, 1)
	c.Close()
	_, err := c.Read(b)

	if err != net.ErrClosed {
		t.Errorf("read after close gave err %v instead of %v", err, net.ErrClosed)
	}
}

func TestWrite(t *testing.T) {
	var r, w bytes.Buffer
	c := start(testserv.New(&r, &w))

	msg := []byte("bocchi the rock!")
	n, err := c.Write(msg)

	if err != nil {
		t.Errorf("write failed: %v", err)
	}
	if n != len(msg) {
		t.Errorf("wrote wrong length: want %v, got %v", len(msg), n)
	}
	if string(msg) != w.String() {
		t.Errorf("wrote wrong thing: want %q, got %q", msg, w.String())
	}
}
