/*
Copyright 2022 Branden J Brown

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package tmi

import (
	"bufio"
	"context"
	"errors"
	"fmt"
	"io"
	"net"
	"strconv"
	"strings"
	"time"
)

// ConnectConfig provides connection and user parameters for an IRC connection.
type ConnectConfig struct {
	// Dial is the function used to connect to the network. It is typically the
	// DialContext method of a net.Dialer or tls.Dialer. A value of nil is
	// equivalent to the DialContext method of the zero value of net.Dialer.
	Dial func(ctx context.Context, network, addr string) (net.Conn, error)
	// Address is the IRC server address to pass to Dial. If it is the empty
	// string, then DefaultServerAddr is used instead.
	Address string
	// RetryWait returns the duration to wait before attempting reconnection
	// when the nth attempt to dial fails. If the second returned value is
	// false, reconnection attempts cease. A value of nil means to never
	// reconnect.
	RetryWait func(n int) (time.Duration, bool)
	// Nick is the nickname with which to connect. For Twitch IRC, this should
	// be the username of the account converted to lower case.
	Nick string
	// User is the username with which to connect. If it is the empty string,
	// Nick is used.
	User string
	// Pass is the password with which to connect. For Twitch IRC, this should
	// be a string of the form "oauth:tokengoeshere" using an OAuth token which
	// has at least chat:read and chat:edit scopes.
	Pass string
	// Capabilities is the list of capabilities to request upon connection.
	Capabilities []string
	// Timeout is the timeout to use for reads from and writes to the server.
	// A value of 0 means no timeouts.
	Timeout time.Duration
}

// DefaultServerAddr is the default IRC server address to use when a
// ConnectConfig has no Address. It is the address of the Twitch Messaging
// Interface with a TLS connection. (This means using the zero value for both
// Address and Dial in a ConnectConfig will fail.)
const DefaultServerAddr = "irc.chat.twitch.tv:6697"

func (cfg ConnectConfig) defaults() ConnectConfig {
	if cfg.Dial == nil {
		var d net.Dialer
		cfg.Dial = d.DialContext
	}
	if cfg.Address == "" {
		cfg.Address = DefaultServerAddr
	}
	if cfg.RetryWait == nil {
		cfg.RetryWait = func(n int) (time.Duration, bool) { return 0, false }
	}
	if cfg.User == "" {
		cfg.User = cfg.Nick
	}
	return cfg
}

// Connect connects to an IRC server. It should be used in a go statement. Once
// the connection is finished, Connect closes recv.
//
// Connect automatically handles reconnecting, whether due to network errors or
// due to RECONNECT messages from the server. To disconnect and not reconnect,
// send a QUIT message, cancel the context, or close the send channel.
// Additionally, as a special case, sending a RECONNECT message closes the
// connection and reconnects.
func Connect(ctx context.Context, cfg ConnectConfig, log Logger, send <-chan *Message, recv chan<- *Message) error {
	cfg = cfg.defaults()
	if log == nil {
		log = nilLogger{}
	}
	defer close(recv)
	sem := make(chan struct{}, 1)
	ctx, cancel := context.WithCancel(ctx)
	for ctx.Err() == nil {
		conn, err := connDial(ctx, cfg, log)
		if err != nil {
			// Connection failed. connDial logged the reason.
			cancel()
			return err
		}
		// We need a per-connection context so that the receiver goroutine can
		// signal to the sender when the connection closes, especially due to a
		// connection error or RECONNECT. Otherwise, the sender would sit in a
		// select statement indefinitely.
		sendCtx, sendCancel := context.WithCancel(ctx)
		go func() {
			// NOTE(zeph): This sets the error in the enclosing loop.
			err = connRecv(ctx, cfg, log, conn, recv)
			sendCancel()
			sem <- struct{}{}
		}()
		connSend(sendCtx, cfg, log, cancel, conn, send)

		// Wait for the receiver goroutine. Rather than a waitgroup, we use a
		// buffered channel as a counting semaphore so that we can select with
		// the context cancellation.
		select {
		case <-ctx.Done():
			// Context closed. Close the connection so the reader and writer
			// unblock, then receive a value from the semaphore in place of the
			// one we'd normally receive on the other case.
			if err := conn.Close(); err != nil {
				log.Error(err)
			}
			<-sem
		case <-sem: // do nothing
		}

		// Check the error from connRecv. It's nil if e.g. we receive a
		// RECONNECT from the server.
		if err != nil {
			cancel()
			if err == io.EOF {
				err = io.ErrUnexpectedEOF
			}
			return err
		}
	}
	err := ctx.Err()
	cancel()
	return err
}

func connDial(ctx context.Context, cfg ConnectConfig, log Logger) (net.Conn, error) {
	i := 0
	for {
		conn, err := cfg.Dial(ctx, "tcp", cfg.Address)
		// NOTE that this error check condition is inverted! The remainder of
		// the code is retry logic, not the happy path.
		if err == nil {
			return conn, nil
		}
		log.Error(err)
		wait, ok := cfg.RetryWait(i)
		if !ok {
			log.Error(errors.New("out of retries, giving up"))
			return nil, err
		}
		tmr := time.NewTimer(wait)
		select {
		case <-ctx.Done():
			return nil, ctx.Err()
		case <-tmr.C:
			i++
			log.Status("attempt " + strconv.Itoa(i+1) + " to connect to " + cfg.Address)
		}
	}
}

func connSend(
	ctx context.Context,
	cfg ConnectConfig,
	log Logger,
	cancel context.CancelFunc,
	conn net.Conn,
	send <-chan *Message,
) {
	defer func() {
		if err := conn.Close(); err != nil {
			log.Error(fmt.Errorf("error closing connection in sender: %w", err))
		}
	}()
	write := func(msg string) error {
		log.Send(msg)
		if cfg.Timeout != 0 {
			conn.SetWriteDeadline(time.Now().Add(cfg.Timeout))
		}
		_, err := io.WriteString(conn, msg)
		return err
	}

	// TODO(zeph): perhaps login messages should go in connDial to make testing easier
	if len(cfg.Capabilities) != 0 {
		s := strings.Join(cfg.Capabilities, " ")
		if err := write("CAP REQ :" + s + "\r\n"); err != nil {
			log.Error(err)
			return
		}
	}
	// Send login info as a single string.
	login := "PASS " + cfg.Pass + "\r\nNICK " + cfg.Nick + "\r\nUSER " + cfg.User + "\r\n"
	if err := write(login); err != nil {
		log.Error(err)
		return
	}

	for {
		select {
		case <-ctx.Done():
			log.Status("sender: context closed")
			return
		case msg, ok := <-send:
			if !ok {
				log.Status("sender: no more messages")
				cancel()
				write("QUIT :goodbye\r\n") // error doesn't matter
				return
			}
			if msg == nil {
				continue
			}
			switch msg.Command {
			case "QUIT":
				cancel()
				write(msg.String() + "\r\n") // error doesn't matter
				return
			case "RECONNECT":
				m := msg.Trailing
				if m == "" {
					m = "goodbye"
				}
				write("QUIT :" + m + "\r\n") // error doesn't matter
				return
			default:
				err := write(msg.String() + "\r\n")
				if err != nil {
					log.Error(err)
					return
				}
			}
		}
	}
}

func connRecv(
	ctx context.Context,
	cfg ConnectConfig,
	log Logger,
	conn net.Conn,
	recv chan<- *Message,
) error {
	defer func() {
		if err := conn.Close(); err != nil && !errors.Is(err, net.ErrClosed) {
			log.Error(fmt.Errorf("error closing connection in receiver: %w", err))
		}
	}()
	r := bufio.NewReaderSize(conn, tagLimit+ircLimit+2)

	for {
		if cfg.Timeout != 0 {
			conn.SetReadDeadline(time.Now().Add(cfg.Timeout))
		}
		msg, err := Parse(r)
		if err != nil {
			if !errors.Is(err, net.ErrClosed) {
				// Don't log operations on closed connections. It closed
				// because we asked it to.
				log.Error(fmt.Errorf("error while receiving: %w", err))
			}
			if _, ok := err.(*Malformed); ok {
				continue
			}
			return err
		}

		switch msg.Command {
		case "RECONNECT":
			log.Recv(msg.String())
			log.Status("received RECONNECT, closing connection")
			return nil
		case "PING":
			log.Ping(msg.String())
			s := "PONG :" + msg.Trailing + "\r\n"
			log.Ping(s[:len(s)-2]) // strip \r\n from log
			// TODO: should we send the PONG to the sender instead?
			if cfg.Timeout != 0 {
				conn.SetWriteDeadline(time.Now().Add(cfg.Timeout))
			}
			if _, err := io.WriteString(conn, s); err != nil {
				log.Error(fmt.Errorf("error responding to PING: %w", err))
				return err
			}
			// Check context for cancellation.
			if err := ctx.Err(); err != nil {
				return err
			}
		case "NOTICE":
			if len(msg.Params) == 1 && msg.Params[0] == "*" && msg.Trailing == "Login authentication failed" {
				log.Error(ErrAuthenticationFailed)
				return ErrAuthenticationFailed
			}
			fallthrough
		default:
			log.Recv(msg.String())
			select {
			case <-ctx.Done():
				return ctx.Err()
			case recv <- msg:
				// do nothing
			}
		}
	}
}

// ErrAuthenticationFailed is the error returned from [Connect] when a
// a connection
var ErrAuthenticationFailed = errors.New("login authentication failed")

// RetryList returns a function suitable for a ConnectConfig's RetryWait which
// yields the durations from durs. If forever is true, the last duration is
// repeated forever; otherwise, reconnection attempts end once the list is
// exhausted. RetryList panics if durs is empty.
func RetryList(forever bool, durs ...time.Duration) func(int) (time.Duration, bool) {
	if len(durs) == 0 {
		panic("tmi: empty retry wait list")
	}
	// Make a copy of durs.
	d := append(([]time.Duration)(nil), durs...)
	if forever {
		return func(i int) (time.Duration, bool) {
			if i >= len(d) {
				return d[len(d)-1], true
			}
			return d[i], true
		}
	}
	return func(i int) (time.Duration, bool) {
		if i >= len(d) {
			return 0, false
		}
		return d[i], true
	}
}
