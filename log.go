/*
Copyright 2022 Branden J Brown

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package tmi

import "log"

// Logger logs all messages related to an IRC connection.
//
// A Logger should be limited to displaying messages. It should not be used to
// implement message handling logic. A Logger must be safe for concurrent use.
type Logger interface {
	// Error logs an error, either with the network or with parsing a message.
	Error(error)
	// Status logs a status message, which includes connection attempts,
	// reasons for disconnecting, and possibly other messages.
	Status(string)
	// Send logs the text of a non-trivial IRC message sent to the server.
	Send(string)
	// Recv logs the text of a non-trivial IRC message received from the server.
	Recv(string)
	// Ping logs trivial PING and PONG messages between the server and client.
	Ping(string)
}

// Log returns a Logger which wraps a log.Logger. Each method prefixes the
// logged line with the message type. Pings are logged only if pings is true.
func Log(logger *log.Logger, pings bool) Logger {
	if pings {
		return &pingLogger{basicLogger{logger}}
	}
	return &basicLogger{log: logger}
}

type basicLogger struct {
	log *log.Logger
}

func (l *basicLogger) Error(err error)   { l.log.Println("ERROR:", err) }
func (l *basicLogger) Status(msg string) { l.log.Println("Status:", msg) }
func (l *basicLogger) Send(msg string)   { l.log.Println("Send:", msg) }
func (l *basicLogger) Recv(msg string)   { l.log.Println("Recv:", msg) }
func (l *basicLogger) Ping(msg string)   {}

type pingLogger struct {
	basicLogger
}

func (l *pingLogger) Ping(msg string) {
	l.log.Println("Ping:", msg)
}

// nilLogger is a Logger with no output.
type nilLogger struct{}

func (nilLogger) Error(error)   {}
func (nilLogger) Status(string) {}
func (nilLogger) Send(string)   {}
func (nilLogger) Recv(string)   {}
func (nilLogger) Ping(string)   {}
