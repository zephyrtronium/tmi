/*
Copyright 2022 Branden J Brown

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package tmi

import (
	"bytes"
	"context"
	"embed"
	"io"
	"net"
	"os"
	"regexp"
	"testing"
	"time"

	"gitlab.com/zephyrtronium/tmi/testserv"
)

//go:embed testdata/*.server
var testdata embed.FS

func TestConnDefaults(t *testing.T) {
	r, err := testdata.Open("testdata/basic.server")
	if err != nil {
		t.Fatal(err)
	}
	defer r.Close()
	var w bytes.Buffer
	srv := testserv.New(r, &w)
	cfg := ConnectConfig{
		Dial: func(ctx context.Context, network, addr string) (net.Conn, error) {
			if addr != DefaultServerAddr {
				t.Errorf("wrong dial addr: want %q, got %q", DefaultServerAddr, addr)
			}
			return srv, nil
		},
		Nick: "bocchi",
		Pass: "oauth:ryou",
	}
	send, recv := make(chan *Message), make(chan *Message)
	wait := make(chan struct{})
	go func() {
		Connect(context.Background(), cfg, nil, send, recv)
		close(wait)
	}()
	<-wait

	srv.Close()
	wrote := w.String()
	want := []string{
		`(?m)^NICK bocchi\r?$`,
		`(?m)^USER bocchi\r?$`,
		`(?m)^PASS oauth:ryou\r?$`,
		`(?m)^PONG :1\r?$`,
	}
	doNotWant := []string{
		`(?m)^CAP REQ`,
	}
	for _, p := range want {
		match, err := regexp.MatchString(p, wrote)
		if err != nil {
			t.Fatalf("error parsing %q: %v", p, err)
		}
		if !match {
			t.Errorf("no match of %q", p)
		}
	}
	for _, p := range doNotWant {
		match, err := regexp.MatchString(p, wrote)
		if err != nil {
			t.Fatalf("error parsing %q: %v", p, err)
		}
		if match {
			t.Errorf("unexpected match of %q", p)
		}
	}
	if t.Failed() {
		t.Logf("written data:\n%s", wrote)
	}
	// TODO(zeph): check default RetryWait logic
}

func TestConnReconnect(t *testing.T) {
	r, err := testdata.Open("testdata/reconnect.server")
	if err != nil {
		t.Fatal(err)
	}
	defer r.Close()
	var w bytes.Buffer
	dials := 0
	srv := testserv.New(r, &w)
	cfg := ConnectConfig{
		Dial: func(ctx context.Context, network, addr string) (net.Conn, error) {
			dials++
			// Always return an error after some number of attempts so that
			// Connect returns.
			if dials > 20 {
				return nil, io.EOF
			}
			if dials%2 == 0 {
				return nil, os.ErrDeadlineExceeded
			}
			return srv, nil
		},
		RetryWait: RetryList(false, time.Millisecond, time.Millisecond, time.Millisecond),
		Nick:      "bocchi",
		User:      "ryou",
		Pass:      "oauth:kita",
	}
	send, recv := make(chan *Message), make(chan *Message)
	go Connect(context.Background(), cfg, nil, send, recv)

	login := []*regexp.Regexp{
		regexp.MustCompile(`(?m)^PASS oauth:kita\r?$`),
		regexp.MustCompile(`(?m)^NICK bocchi\r?$`),
		regexp.MustCompile(`(?m)^USER ryou\r?$`),
	}
	for range recv {
	}
	srv.Close()
	wrote := w.String()
	for _, r := range login {
		m := r.FindAllString(wrote, -1)
		if len(m) != 2 {
			t.Errorf("wrong number of %q matches: want 2, got %d", r.String(), len(m))
		}
	}
}

func TestConnCaps(t *testing.T) {
	r, err := testdata.Open("testdata/message.server")
	if err != nil {
		t.Fatal(err)
	}
	defer r.Close()
	var w bytes.Buffer
	srv := testserv.New(r, &w)
	cfg := ConnectConfig{
		Dial:         func(ctx context.Context, network, addr string) (net.Conn, error) { return srv, nil },
		Nick:         "bocchi",
		Pass:         "oauth:ryou",
		Capabilities: []string{"nijika", "kita"},
	}
	send, recv := make(chan *Message), make(chan *Message)
	wait := make(chan struct{})
	go func() {
		Connect(context.Background(), cfg, nil, send, recv)
		close(wait)
	}()
	// get a message from the server so we know we've had a chance to write
	<-recv
	srv.Close()
	<-wait

	wrote := w.String()
	ok, err := regexp.MatchString(`(?m)^CAP REQ :nijika kita\r?$`, wrote)
	if err != nil {
		t.Fatal(err)
	}
	if !ok {
		t.Errorf("wrong or no CAP REQ in written data:\n%s", wrote)
	}
}

func TestConnQuit(t *testing.T) {
	cases := []struct {
		name   string
		method func(send chan *Message, cancel context.CancelFunc)
	}{
		{
			"QUIT",
			func(send chan *Message, cancel context.CancelFunc) {
				send <- &Message{Command: "QUIT", Trailing: "goodbye"}
			},
		},
		{
			"close",
			func(send chan *Message, cancel context.CancelFunc) {
				close(send)
			},
		},
		{
			"cancel",
			func(send chan *Message, cancel context.CancelFunc) {
				cancel()
			},
		},
	}
	for _, c := range cases {
		t.Run(c.name, func(t *testing.T) {
			r, err := testdata.Open("testdata/basic.server")
			if err != nil {
				t.Fatal(err)
			}
			defer r.Close()
			srv := testserv.New(r, io.Discard)
			cfg := ConnectConfig{
				Dial:      func(ctx context.Context, network, addr string) (net.Conn, error) { return srv, nil },
				RetryWait: RetryList(true, 15*time.Millisecond),
				Nick:      "bocchi",
				Pass:      "oauth:ryou",
				Timeout:   time.Second,
			}
			ctx, cancel := context.WithCancel(context.Background())
			send, recv := make(chan *Message), make(chan *Message)
			wait := make(chan struct{})
			go func() {
				err = Connect(ctx, cfg, nil, send, recv)
				close(wait)
			}()
			c.method(send, cancel)
			<-wait
			// The failure condition here is that Connect doesn't return and
			// the test times out. We could try to check that we sent a QUIT in
			// the cases that should send one (which excludes canceling the
			// context), but then we'd have to care about how much data gets
			// written if the test is failing.
		})
	}
}

func TestConnSendReconnect(t *testing.T) {
	t.Skip("FIXME: test fails due to bad sync")

	dials := 0
	var w bytes.Buffer
	cfg := ConnectConfig{
		Dial: func(ctx context.Context, network, addr string) (net.Conn, error) {
			dials++
			r, err := testdata.Open("testdata/message.server")
			if err != nil {
				t.Fatal(err)
			}
			srv := testserv.New(r, &w)
			return srv, nil
		},
		RetryWait: RetryList(true, 15*time.Millisecond),
		Nick:      "bocchi",
		Pass:      "oauth:ryou",
	}
	send, recv := make(chan *Message), make(chan *Message)
	wait := make(chan struct{})
	go func() {
		Connect(context.Background(), cfg, nil, send, recv)
		close(wait)
	}()
	send <- &Message{Command: "RECONNECT", Trailing: "test reconnect"}
	for range recv {
	}
	<-wait

	wrote := w.String()
	if dials != 2 {
		t.Errorf("wrong number of dials: want 2, got %d", dials)
	}
	ok, err := regexp.MatchString(`(?m)^QUIT :test reconnect\r?$`, wrote)
	if err != nil {
		t.Fatal(err)
	}
	if !ok {
		t.Errorf("didn't send correct QUIT:\n%s", wrote)
	}
}

func TestAuthFailed(t *testing.T) {
	r, err := testdata.Open("testdata/auth.server")
	if err != nil {
		t.Fatal(err)
	}
	defer r.Close()
	srv := testserv.New(r, io.Discard)
	cfg := ConnectConfig{
		Dial:      func(ctx context.Context, network, addr string) (net.Conn, error) { return srv, nil },
		RetryWait: RetryList(true, 15*time.Millisecond),
		Nick:      "bocchi",
		Pass:      "oauth:ryou",
		Timeout:   time.Second,
	}
	send, recv := make(chan *Message), make(chan *Message)
	err = Connect(context.Background(), cfg, nil, send, recv)
	if err != ErrAuthenticationFailed {
		t.Errorf("wrong error from connect: want ErrAuthenticationFailed, got %v", err)
	}
}

func TestRetryList(t *testing.T) {
	cases := []struct {
		name string
		vals []time.Duration
	}{
		{"one", []time.Duration{1}},
		{"many", []time.Duration{1, 2, 3, 4, 5}},
	}
	for _, c := range cases {
		t.Run(c.name, func(t *testing.T) {
			fn := RetryList(false, c.vals...)
			for i, w := range c.vals {
				k, ok := fn(i)
				if !ok {
					t.Errorf("early stop in non-forever list at %d iters", i)
				}
				if k != w {
					t.Errorf("wrong wait in non-forever list: want %v, got %v", w, k)
				}
			}
			_, ok := fn(len(c.vals))
			if ok {
				t.Error("non-forever list has too many values")
			}

			fn = RetryList(true, c.vals...)
			for i, w := range c.vals {
				k, ok := fn(i)
				if !ok {
					t.Errorf("forever list stopped at %d iters", i)
				}
				if k != w {
					t.Errorf("wrong wait in forever list: want %v, got %v", w, k)
				}
			}
			k, ok := fn(len(c.vals))
			if !ok {
				t.Error("forever list stopped yielding values")
			}
			if w := c.vals[len(c.vals)-1]; k != w {
				t.Errorf("wrong wait at end of forever list: want %v, got %v", w, k)
			}
		})
	}

	t.Run("zero", func(t *testing.T) {
		defer func() {
			if err := recover(); err == nil {
				t.Error("no panic with empty argument list")
			}
		}()
		RetryList(false)
	})
}
