/*
Copyright 2022 Branden J Brown

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

// Package tmi provides an efficient IRC client interface.
//
// While package tmi is designed for the Twitch Messaging Interface (TMI,
// a.k.a. Twitch.TV IRC or just Twitch Chat), it should be able to handle
// any valid RFC 1459 messages plus IRCv3 tags. The focus is on simplicity,
// universality, and performance, rather than convenience.
//
package tmi

import (
	"fmt"
	"strconv"
	"strings"
	"time"
)

// Message represents a single IRC message.
type Message struct {
	// Tags is the full tags component of the received message. Use the Tag
	// method to get the parsed, unquoted value of a single tag.
	Tags string
	// Sender is identifying information of the user or server that sent the
	// message.
	Sender
	// Command is the message command or numeric response code.
	Command string
	// Params is the "middle" parameters of the message.
	Params []string
	// Trailing is the "trailing" parameter of the message.
	Trailing string
}

// Privmsg is a shortcut to create a PRIVMSG message for sending.
func Privmsg(to, message string) *Message {
	return &Message{Command: "PRIVMSG", Params: []string{to}, Trailing: message}
}

// Whisper is a shortcut to create a Twitch whisper to a given user.
func Whisper(to, message string) *Message {
	return &Message{
		Command:  "PRIVMSG",
		Params:   []string{"#jtv"},
		Trailing: "/w " + to + " " + message,
	}
}

// Respond creates an appropriately-targeted response to a PRIVMSG or WHISPER.
// Panics if the message type is neither PRIVMSG nor WHISPER. The message is
// formatted according to the rules of fmt.Sprintf.
//
// To reply directly to a message, e.g. with threading, use Reply instead.
func (m *Message) Respond(format string, args ...interface{}) *Message {
	msg := strings.TrimSpace(fmt.Sprintf(format, args...))
	switch m.Command {
	case "PRIVMSG":
		return Privmsg(m.To(), msg)
	case "WHISPER":
		return Whisper(m.Nick, msg)
	default:
		panic("tmi: cannot respond to " + m.String())
	}
}

// Reply creates a possibly threaded response to a PRIVMSG or WHISPER.
// Panics if the message type is neither PRIVMSG nor WHISPER. The message is
// formatted according to the rules of fmt.Sprintf.
//
// To create a response in the same channel or to the same user as a message
// but without threading, use Respond instead.
func (m *Message) Reply(format string, args ...interface{}) *Message {
	r := m.Respond(format, args...)
	id, ok := m.Tag("id")
	if !ok {
		return r
	}
	// TODO: should quote id, maybe don't overwrite other tags
	r.Tags = "reply-parent-msg-id=" + id
	return r
}

// String formats the message as an IRC message string appropriate for sending
// to an IRC server, not including the ending CR LF sequence. This does not
// perform any validation.
func (m *Message) String() string {
	var b strings.Builder
	if len(m.Tags) != 0 {
		b.WriteByte('@')
		b.WriteString(m.Tags)
		b.WriteByte(' ')
	}
	snd := m.Sender.String()
	if snd != "" {
		b.WriteByte(':')
		b.WriteString(snd)
		b.WriteByte(' ')
	}
	b.WriteString(m.Command)
	for _, p := range m.Params {
		b.WriteByte(' ')
		b.WriteString(p)
	}
	if m.Trailing != "" {
		b.WriteByte(' ')
		b.WriteByte(':')
		b.WriteString(m.Trailing)
	}
	return b.String()
}

// Tag retrieves a tag by name. ok is false if and only if the tag is not
// present.
func (m *Message) Tag(name string) (val string, ok bool) {
	tags := m.Tags
	for tags != "" {
		k := strings.IndexByte(tags, ';')
		tag := tags
		if k >= 0 {
			tag = tags[:k]
			tags = tags[k+1:]
		} else {
			tags = ""
		}
		k = strings.IndexByte(tag, '=')
		var key, val string
		if k >= 0 {
			key = tag[:k]
			val = tag[k+1:]
		} else {
			key = tag
		}
		if key == name {
			return unquoteTag(val), true
		}
	}
	return "", false
}

// ForeachTag calls f for each tag on m. Tag values are unquoted and may be the
// empty string.
func (m *Message) ForeachTag(f func(key, value string)) {
	tags := m.Tags
	for tags != "" {
		k := strings.IndexByte(tags, ';')
		tag := tags
		if k >= 0 {
			tag = tags[:k]
			tags = tags[k+1:]
		} else {
			tags = ""
		}
		k = strings.IndexByte(tag, '=')
		var key, val string
		if k >= 0 {
			key = tag[:k]
			val = tag[k+1:]
		} else {
			key = tag
		}
		f(key, unquoteTag(val))
	}
}

// Badges appends the list of Twitch badges, parsed from the badges tag and
// without versions, to v and returns v.
func (m *Message) Badges(v []string) []string {
	bb, _ := m.Tag("badges")
	for bb != "" {
		// Index rather than use Split to avoid unnecessary allocations.
		k := strings.IndexByte(bb, ',')
		b := bb
		if k >= 0 {
			b = b[:k]
			bb = bb[k+1:]
		} else {
			bb = ""
		}
		k = strings.IndexByte(b, '/')
		// We should always enter this branch, but it isn't worth panicking
		// if we don't.
		if k >= 0 {
			b = b[:k]
		}
		v = append(v, b)
	}
	return v
}

// Time returns the time at which the message was sent, according to the
// tmi-sent-ts tag.
func (m *Message) Time() time.Time {
	tag, _ := m.Tag("tmi-sent-ts")
	ts, err := strconv.ParseInt(tag, 10, 64)
	if err != nil {
		return time.Time{}
	}
	return time.UnixMilli(ts)
}

// To returns m.Params[0]. Panics if m.Params is empty.
//
// Notably, this identifies the channel or user a PRIVMSG message is sent to.
func (m *Message) To() string {
	return m.Params[0]
}

// DisplayName returns a display name for the message sender. If the message
// has a non-empty display-name tag, then this returns that; otherwise, it
// returns the sender's nick.
func (m *Message) DisplayName() string {
	if n, _ := m.Tag("display-name"); n != "" {
		return n
	}
	return m.Nick
}

// Sender is a message Sender. It may represent a user or server.
type Sender struct {
	// Nick is the nickname of the user who produced the message, or the
	// hostname of the server for messages not produced by users.
	Nick string
	// User is the username of the user who produced the message, if any. For
	// Twitch IRC, this is always the same as Nick if it is nonempty.
	User string
	// Host is the hostname of the user who produced the message, if any. For
	// Twitch IRC, this is always "tmi.twitch.tv" or "<user>.tmi.twitch.tv",
	// where <user> is the username of the authenticated client.
	Host string
}

// String formats the sender as "nick!user@host". Separators are omitted for
// empty fields where valid.
func (s Sender) String() string {
	if s.Host != "" {
		if s.User != "" {
			return s.Nick + "!" + s.User + "@" + s.Host
		}
		return s.Nick + "@" + s.Host
	}
	return s.Nick
}
